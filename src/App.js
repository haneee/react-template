import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import Mainpage from "./pages/Mainpage";
import Detailpage from "./pages/Detailpage";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route path="/mainpage" element={<Mainpage />} />
          <Route path="/detail" element={<Detailpage />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
